@extends('layouts.app')

@section('content')
<div class="jumbotron">
            <h3>{{$name}}</h3>
            @if(count($service) > 0)
            <ul class="list-group">
                @foreach($service as $serv)
                    <li>{{$serv}}</li>
                @endforeach
            </ul>
            @endif
        </div>
@endsection
