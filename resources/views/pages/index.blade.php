@extends('layouts.app')

@section('content')
        <div class="jumbotron text-center">
            <h2>{{$title}}</h2>
            <p>This is the main page of the laravel tuto</p>
            <p><a class="btn btn-primary" href="{{ route('login') }}">Login</a> <a href="{{ route('register') }}" class="btn btn-danger" href="/register">Register</a></p> 
        </div>
@endsection
