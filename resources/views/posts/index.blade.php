@extends('layouts.app')

@section('content')
<div style="background:white" class="jumbotron ">
    <h2>Posts</h2>
    @if(count($posts) > 0)
        @foreach($posts as $post)
            <div class="row well">
                <div class="col-md-4 col-sm-4">
                    <img style="width:100%" src="/storage/cover_images/{{$post->cover_image}}" >
                </div>
                <div class="co-md-8 col-sm-8">
                        <h3><a href="/posts/{{$post->id}}">{{$post->title}}</a></h3>
                        <small>Created on {{$post->created_at}} by {{$post->user->name}}</small>
                </div>
            </div>
        @endforeach
    </ul>
    {{$posts->links()}}
    @endif
      </div>
@endsection