@extends('layouts.app')

@section('content')
<div style="background:white" class="jumbotron ">
<a href="/posts" class="btn btn-default">Go back</a>
    <div class="container">
        <strong><h2>{{$post->title}}</h2><strong>
        <small>Created on {{$post->created_at}} by {{$post->user->name}}</small> <br/>
        <hr>
        <img style="width:100%" src="/storage/cover_images/{{$post->cover_image}}" >
          <hr>
        <div>{!!$post->body!!}</div>
        <small>Created on {{$post->created_at}} by {{$post->user->name}}</small>
        @include('laravelLikeComment::like', ['like_item_id' => 'image_31'])
    </div>
        @include('laravelLikeComment::comment', ['comment_item_id' => 'video_12'])
    <br>
    @if(!auth::guest())
        @if(auth::user()->id == $post->user_id)
            <a href="/posts/{{$post->id}}/edit" class="btn btn-info">Edit post</a>
            {!! Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'post', 'class' => 'pull-right']) !!}
            {{form::hidden('_method', 'DELETE')}}
                <div class="col-md-6 form-group">
                    {{form::submit('Delete', ['class' => 'btn btn-danger'])}}
                </div>
            {!! Form::close() !!}
        @endif
    @endif
    </div>
@endsection