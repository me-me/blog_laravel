@extends('layouts.app')

@section('content')
    <h2>Create a new post</h2>
    {!! Form::open(['action' => 'PostsController@store', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
        <div class="col-md-6 form-group">
            {{form::label('title', 'Title')}}
            {{form::text('title', '', ['class' => 'form-control', 'placeholder' => 'input post title'])}}
        </div>
        <div class="col-md-12 form-group">
            {{form::label('title', 'Body')}}
            {{form::textarea('body', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'input post body'])}}
        </div>
        <div class="col-md-12 form-group">
            {{form::file('cover_image')}}
        </div>
        <div class="col-md-6 form-group">
            {{form::submit('Submit', ['class' => 'btn btn-primary'])}}
        </div>
    {!! Form::close() !!}
@endsection 