@extends('layouts.app   ')

@section('content')
    <h2>Edit this post</h2>
    {!! Form::open(['action' => ['PostsController@update', $post->id],  'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
        <div class="col-md-6 form-group">
            {{form::label('title', 'Title')}}
            {{form::text('title', $post->title, ['class' => 'form-control'])}}
        </div>
        <div class="col-md-12 form-group">
            {{form::label('title', 'Body')}}
            {{form::textarea('body', $post->body, ['id' => 'article-ckeditor', 'class' => 'form-control'])}}
        </div>
         <div class="col-md-12 form-group">
            {{form::file('cover_image')}}
        </div>
        <div class="col-md-6 form-group">
        {{form::hidden('_method', 'PUT')}}
            {{form::submit('Submit', ['class' => 'btn btn-primary'])}}
        </div>
    {!! Form::close() !!}
@endsection