@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a href="/posts/create" class="btn btn-primary">Create post</a>
                    <h3>Your blog posts</h3>
                    @if(count($posts) > 0)
                    <table class="table table-striped">
                        <thead>
                        </thead>
                            <th>title</th>
                            <th></th>
                            <th></th>
                        <tbody>
                            @foreach($posts as $post)
                            <tr>
                                <td>{{$post->title}}</td>
                                <td><a href="/posts/{{$post->id}}/edit" class="btn btn-primary pull-right">Edit</a></td>
                                <td>{!! Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'post']) !!}
                                {{form::hidden('_method', 'DELETE')}}
                                    <div class="col-md-6 form-group">
                                        {{form::submit('Delete', ['class' => 'btn btn-danger'])}}
                                    </div>
                                {!! Form::close() !!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        <p>You Don't have any posts..</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
