<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
    return ('Bonjour');
});


//return pages
Route::get('/about/', function (){
    return view('pages.about');
});


//Passe argument
Route::get('/user/{id}', function ($id){
    return 'this user is '.$id;
});

//Pass multiplae argument
Route::get('users/{id}/{name}', function($name, $id){
    return 'This is '.$name.' My id is '.$id;
});
*/

Route::get('/', 'PagesController@index');
Route::get('/services', 'PagesController@services');
Route::get('/about', 'PagesController@about');

Route::resource('posts', 'PostsController');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
