<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    //
    public function index(){
        $title = 'I am yixuan WANG welcome to my blog';
        return view('pages.index', compact('title'));
    }
    public function about(){
        $title = 'This is the abdout page';
        return view('pages.about')->with('title', $title);
    }
    public function services(){
        $data = array(
            'name' => 'tesla',
            'service' => ['web', 'developer', 'programmer']
        );
        return view('pages.services')->with($data);
    }
}
