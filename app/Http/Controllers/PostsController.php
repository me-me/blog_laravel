<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
//bring the post model
use \App\Post;
//Bring the db for sal syntax
use DB;

class PostsController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Return all posts
        //$post = Post::all();
        
        //Order by elequent
        //$post = post::orderBy('title', 'desc')->get();

        //Create pagination
        $post = post::orderBy('created_at', 'desc')->paginate(10);
        //Use sa syntax
        //$post = db::select('SELECT * FROM posts');
        
        //Return the view posts index file
        return view('posts.index')->with('posts', $post);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Create a view
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate 
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:1999'
        ]);

        //hundle file upload 
        if ($request->hasFile('cover_image')) {
            //Save ithe complete file 
            $fileNameWithEx = $request->file('cover_image')->getClientOriginalName();
            //File name
            $fileName = pathinfo($fileNameWithEx, PATHINFO_FILENAME);
            //Goet extention
            $fileEx = $request->file('cover_image')->getClientOriginalExtension();
            //File anme to store
            $filenameToStore = $fileName.'_'.time().'_'.$fileEx;
            //upload image
            $path = $request->file('cover_image')->storeAs('public/cover_images', $filenameToStore); 
        } else {
            $filenameToStore = 'no-image.jpg';
        };

        $post = new Post;
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->user()->id;
        $post->cover_image = $filenameToStore;
        $post->save();
        return redirect('/posts')->with('success', 'Post created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Show posts 
        $post = post::find($id);
        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {    
        //Edit my posts
        $post = post::find($id);

        //Access control
        if (auth()->user()->id != $post->user_id)
            return redirect('/posts')->with('error', 'Unauthorized page');
        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //Validate 
         $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:1999'
            ]);
    
            //hundle file upload 
            if ($request->hasFile('cover_image')) {
                //Save ithe complete file 
                $fileNameWithEx = $request->file('cover_image')->getClientOriginalName();
                //File name
                $fileName = pathinfo($fileNameWithEx, PATHINFO_FILENAME);
                //Goet extention
                $fileEx = $request->file('cover_image')->getClientOriginalExtension();
                //File anme to store
                $filenameToStore = $fileName.'_'.time().'_'.$fileEx;
                //upload image
                $path = $request->file('cover_image')->storeAs('public/cover_images', $filenameToStore); 
            };
        //Update posts
        $post = post::find($id);
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->user()->id;
        if ($request->hasFile('cover_image')){
            $post->cover_image = $filenameToStore;
        };
        $post->save();
        return redirect('/posts')->with('success', 'Post updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Delete post
        $post = post::find($id);
        
        //Check the correct user
        if (auth()->user()->id !== $post->user_id)
            return redirect('/posts')->with('error', 'Unauthorized page');

        //Delete storgae
        if ($post->cover_image != 'no-image.jpg'){
            //Delete image
            Storage::Delete('public/cover_images/'.$post->cover_image);
        };

        $post->delete();
        return redirect('/posts')->with('success', 'Post deleted');
    }
}
